package com.nuage.dessin.model;

import java.util.HashSet;
import java.util.Set;
import java.io.Serializable;

public class DessinDTO implements Serializable{
	private int id;
	private String url;
	private String user_name;
	private int id_user;
	private int id_image;
	private Set<Integer> likes = new HashSet<>();
	
	public Set<Integer> getLikes() {
		return likes;
	}
	public void setLikes(Set<Integer> likes) {
		this.likes = likes;
	}
	public DessinDTO(int id, String url, String user_name, int id_user, int id_image,Set<Integer> likes) {
		super();
		this.id = id;
		this.url = url;
		this.user_name = user_name;
		this.id_user = id_user;
		this.id_image = id_image;
		this.likes=likes;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public int getId_image() {
		return id_image;
	}
	public void setId_image(int id_image) {
		this.id_image = id_image;
	}
}
