package com.nuage.dessin.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.nuage.dessin.model.Dessin;

public interface DessinRepository extends CrudRepository<Dessin,Integer> {
	
	@Query(nativeQuery=true,value="SELECT * FROM Dessin d WHERE d.id_user=:id_user AND d.id_image=:id_image")
	Optional<List<Dessin>> findByUserAndImage(@Param("id_image") int id_image,@Param("id_user") int id_user);

	@Query(nativeQuery=true,value="SELECT * FROM Dessin d WHERE d.id_user=:id order by id desc limit :number")
	List<Dessin> getDessinByUser(@Param("id") int id,@Param("number") int number);

	@Query(nativeQuery=true,value="SELECT * FROM Dessin d WHERE d.id_image=:id_image order by id")
	Optional<List<Dessin>> getComment(@Param("id_image") int id_image);
}
