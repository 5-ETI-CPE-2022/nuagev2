package com.nuage.dessin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuage.dessin.model.Dessin;
import com.nuage.dessin.repository.DessinRepository;

@Service
public class LikeStorageService {

	@Autowired
	DessinRepository dessinRepository;
	@Autowired
	DessinStorageInfoService dessinService;
	
	public boolean addLike(int id_user,int id_dessin) {
		Dessin dessin=this.dessinService.getdessinbyid(id_dessin);
		try{
			dessin.addLike(id_user);
			this.dessinRepository.save(dessin);
			return true;
		}
		catch(Exception e) {
			return false;
		}
		
	}
	
	public boolean removeLike(int id_user,int id_dessin) {
		System.out.println("on passe par ici pour delete un like");
		Dessin dessin=this.dessinService.getdessinbyid(id_dessin);
		System.out.println(dessin);
		try {
			System.out.println("on rentre dans le try");
			dessin.removeLike(id_user);
			System.out.println(dessin.getLikes());
			this.dessinRepository.save(dessin);
			return true;
		}
		catch(Exception e) {
			return false;
		}
	}	
}
