package com.nuage.dessin.service;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.nuage.dessin.model.Dessin;

import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.InstanceProfileCredentialsProvider;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

@Service
public class AmazonDessinClient {
	private S3Client s3client;
	
	@Autowired
    private DessinStorageInfoService dessinService;
	
	@Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;
    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    @PostConstruct
    private void initializeAmazon() {
    	S3Client amazonS3Client = S3Client.builder().credentialsProvider(InstanceProfileCredentialsProvider.create()).region(Region.US_EAST_1).build();
       this.s3client = amazonS3Client;
    }
    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
    
    private String generateFileName(MultipartFile multiPart) {
        return new Date().getTime() + "-" + multiPart.getOriginalFilename().replace(" ", "_");
    }
    
    private void uploadFileTos3bucket(String fileName, File file) {
        s3client.putObject(PutObjectRequest.builder().bucket(this.bucketName).key(fileName).build(),RequestBody.fromFile(file));
    }
    
    public String uploadFile(MultipartFile multipartFile,int id_image,int id_user,String tag) {

        String fileUrl = "";
        try {
            File file = convertMultiPartToFile(multipartFile);
            String fileName = generateFileName(multipartFile);
            fileUrl = endpointUrl + "/" + bucketName + "/" + fileName;
            this.dessinService.addImage(new Dessin(fileUrl,id_user,id_image,tag));
            uploadFileTos3bucket(fileName, file);
            file.delete();
            return fileUrl;
        } catch (Exception e) {
           e.printStackTrace();
        }
        return null;
    }
	/*public @ResponseBody byte[] dowloadImage(int id) throws IOException {
		Optional<Dessin> imageModel= this.dessinService.dessinById(id);
		if (imageModel.isPresent()) {
			ResponseInputStream<GetObjectResponse> object = this.s3client.getObject(GetObjectRequest.builder()
				    .bucket(this.bucketName)
				    .key(imageModel.get().getUrl())
				    .build());
			return IOUtils.toByteArray(object);
		}
		return null;
	}*/
    
    /*public String deleteFileFromS3Bucket(String fileUrl) {
        String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
        s3client.deleteObject(new DeleteObjectRequest(bucketName + "/", fileName));
        return "Successfully deleted";
    }*/
}
