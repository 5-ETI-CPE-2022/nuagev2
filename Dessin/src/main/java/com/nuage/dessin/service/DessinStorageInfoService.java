package com.nuage.dessin.service;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Collections;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nuage.dessin.model.Dessin;
import com.nuage.dessin.model.DessinDTO;
import com.nuage.dessin.repository.DessinRepository;
import com.nuage.dessin.model.User;

@Service
public class DessinStorageInfoService {

	@Autowired
	DessinRepository dessinRepository;
	
	public List<Dessin> dessinFromUserAndImage(int id_image, int id_user) {
		Optional<List<Dessin>> dessins=this.dessinRepository.findByUserAndImage(id_image,id_user);
		if(dessins.isPresent()) {
			return dessins.get();
		}
		return null;
	}
	
	public void addImage(Dessin dessin) {
		dessinRepository.save(dessin);
	}

	public List<Dessin> getalldessins() {
		return (List<Dessin>) this.dessinRepository.findAll();
	}
	
	public Dessin getdessinbyid(int id) {
		Optional<Dessin> optDessin=this.dessinRepository.findById(id);
		if(optDessin.isPresent())
			return optDessin.get();
		else return null;
	}
	public void delete(int id) {
		this.dessinRepository.deleteById(id);
		
	}
	public List<Dessin> getDessinByUser(int id,int number) {
		// TODO Auto-generated method stub
		return this.dessinRepository.getDessinByUser(id,number);
	}
	
	public List<DessinDTO> getComments(int id) throws URISyntaxException, IOException, InterruptedException{
		Optional<List<Dessin>> listDessin=this.dessinRepository.getComment(id);
		ObjectMapper userMapper = new ObjectMapper();
		List<DessinDTO> dessinDTO=new ArrayList<>();
		if(listDessin.isPresent()) {
			List<Dessin> dessins = listDessin.get();
			if(dessins.size()>1) {
				dessins.remove(0);
				for(Dessin d:dessins) {
					HttpRequest requestUser = HttpRequest.newBuilder(new URI("http://52.4.247.20:8081/user/"+d.getId_user())).build();
					HttpResponse<String> responseUser = HttpClient.newHttpClient().send(requestUser, HttpResponse.BodyHandlers.ofString());
					User associatedUser=userMapper.readValue(responseUser.body().toString(), User.class);
					dessinDTO.add(new DessinDTO(d.getId(),d.getUrl(),associatedUser.getPseudo(),d.getId_user(),d.getId_image(),d.getLikes()));
				}
				return dessinDTO;
			}
			return Collections.emptyList();
		}			
		else
				return Collections.emptyList();
	}
}
