package com.nuage.dessin.controler;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.nuage.dessin.model.Dessin;
import com.nuage.dessin.model.DessinDTO;
import com.nuage.dessin.service.AmazonDessinClient;
import com.nuage.dessin.service.DessinStorageInfoService;
import com.nuage.dessin.service.LikeStorageService;

@RestController
@RequestMapping("/storage/")
public class DessinControler {
    
	private AmazonDessinClient amazonClient;
    private DessinStorageInfoService dessinService;
    private LikeStorageService likeService;
    
    @Autowired
    public DessinControler(AmazonDessinClient amazonClient,DessinStorageInfoService dessinService,LikeStorageService likeService) {
        this.amazonClient = amazonClient;
        this.dessinService=dessinService;
        this.likeService=likeService;
    }


    @PostMapping("/uploadDessin")
    public String uploadDessin(@RequestPart(value = "file") MultipartFile file,@RequestParam String id_user,@RequestParam String id_image,@RequestParam String tag) {
    	System.out.println("on passe par ici pour upload une dessin");
        return this.amazonClient.uploadFile(file,Integer.parseInt(id_image),Integer.parseInt(id_user), tag);
    }
    
    @GetMapping("/getDessin/image/{id_image}/user/{id_user}")
    public List<Dessin> dowloadDessinByUser(@PathVariable String id_image,@PathVariable String id_user){
    	return this.dessinService.dessinFromUserAndImage(Integer.parseInt(id_image),Integer.parseInt(id_user));
    }
    
    @GetMapping("/getDessins")
    public List<Dessin> allDessins() {
    	return this.dessinService.getalldessins();
    }
    
    /*@GetMapping("/dowloadDessin/dessin/{id}")
	public ResponseEntity<byte[]> dowloadDessinById(@PathVariable String id) throws IOException{
		HttpHeaders headers = new HttpHeaders();
    	return new ResponseEntity<>(this.amazonClient.dowloadDessin(Integer.parseInt(id)), headers, HttpStatus.OK);
    }*/

    @GetMapping("/addLike/user/{id_user}/dessin/{id_dessin}")
    public boolean addLike(@PathVariable String id_user,@PathVariable String id_dessin) {
    	return this.likeService.addLike(Integer.parseInt(id_user),Integer.parseInt(id_dessin));
    }
    @GetMapping("/removeLike/user/{id_user}/dessin/{id_dessin}")
    public boolean removeLike(@PathVariable String id_user,@PathVariable String id_dessin) {
    	return this.likeService.removeLike(Integer.parseInt(id_user),Integer.parseInt(id_dessin));
    }
    @GetMapping("/delete/{id}")
    public void delete(@PathVariable String id) {
    	this.dessinService.delete(Integer.parseInt(id));
    }
    @GetMapping("/getDessinByUser/{id}/number/{number}")
    public List<Dessin> dessinByUser(@PathVariable String id,@PathVariable String number){
    	return this.dessinService.getDessinByUser(Integer.parseInt(id),Integer.parseInt(number));
    }
    @GetMapping("/getComments/image/{id}")
    public List<DessinDTO> getComment(@PathVariable String id) throws NumberFormatException, URISyntaxException, IOException, InterruptedException{
    	return this.dessinService.getComments(Integer.parseInt(id));
    }
}
