package com.nuage.dessin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class DessinApplication {

	public static void main(String[] args) {
		SpringApplication.run(DessinApplication.class, args);
	}

}
