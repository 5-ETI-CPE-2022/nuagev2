package com.example.nuages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nuages.databinding.UserPicturesListFragmentBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class UserActivity extends AppCompatActivity {

    ImageButton buttonHome;
    ImageButton buttonCamera;
    ImageButton buttonUser;

    ImageButton buttonEditUser;
    FrameLayout editUserFrame;
    Button editUserValidate;

    ImageView profilePicture;
    TextView username;

    String profilePictureUrl;

    EditText editUsername;
    EditText editEmail;
    EditText editPassword;

    FrameLayout userPictures;
    FrameLayout userDrawings;

    private int userId;
    String userPseudo;
    private Bitmap weatherIcon;

    private int visitUserId;
    private String visitUserPseudo;
    private String origin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);


        buttonHome = findViewById(R.id.user_activity_button_home);
        buttonCamera = findViewById(R.id.user_activity_button_camera);
        buttonUser = findViewById(R.id.user_activity_button_user);

        profilePicture = findViewById(R.id.user_activity_profile_picture);
        username = findViewById(R.id.user_activity_username);

        buttonEditUser = findViewById(R.id.user_activity_edit_user);
        editUserFrame = findViewById(R.id.edit_user_frame);
        editUserValidate = findViewById(R.id.edit_user_button_put);

        editUsername = findViewById(R.id.edit_user_username);
        editEmail = findViewById(R.id.edit_user_email);
        editPassword = findViewById(R.id.edit_user_password);

        userPictures = findViewById(R.id.fragment_container_user_pictures);
        userDrawings = findViewById(R.id.fragment_container_user_drawings);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            weatherIcon = (Bitmap) extras.get("weatherIcon");
            userId = (int) extras.get("userId");
            userPseudo = (String) extras.get("userPseudo");
            origin = (String) extras.get("origin");
            if (origin.equals("myself")) {
                //downloadLastPicture(String.valueOf(userId));
                username.setText(userPseudo);
                getUserInfo(String.valueOf(userId));
            } else {
                visitUserId = (int) extras.get("visitUserId");
                visitUserPseudo = (String) extras.get("visitUserPseudo");
                //downloadLastPicture(String.valueOf(visitUserId));
                username.setText(visitUserPseudo);
                buttonEditUser.setVisibility(View.INVISIBLE);
                getUserInfo(String.valueOf(visitUserId));
            }
        }


        ImageView weatherImageView = findViewById(R.id.weatherIcon_user);
        weatherImageView.setImageBitmap(weatherIcon);

        buttonUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent userActivityIntent = new Intent(UserActivity.this, UserActivity.class);
                UserActivity.this.finish();
                userActivityIntent.putExtra("userId", userId);
                userActivityIntent.putExtra("userPseudo", userPseudo);
                userActivityIntent.putExtra("origin","myself");
                userActivityIntent.putExtra("weatherIcon", weatherIcon);
                UserActivity.this.overridePendingTransition(0, 0);
                startActivity(userActivityIntent);
                UserActivity.this.overridePendingTransition(0, 0);
            }
        });

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivityIntent = new Intent(UserActivity.this, MainActivity.class);
                mainActivityIntent.putExtra("userId", userId);
                mainActivityIntent.putExtra("userPseudo", userPseudo);
                mainActivityIntent.putExtra("weatherIcon", weatherIcon);
                UserActivity.this.overridePendingTransition(0, 0);
                startActivity(mainActivityIntent);
                UserActivity.this.overridePendingTransition(0, 0);
            }
        });

        buttonEditUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editUserFrame.setVisibility(View.VISIBLE);
            }
        });

        editUserValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                putUserEdit();
                editUserFrame.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.user_activity_userpage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editUserFrame.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.edit_user_frame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editUserFrame.setVisibility(View.VISIBLE);
            }
        });

        findViewById(R.id.view_user_pictures).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPictures.setVisibility(View.VISIBLE);
                userDrawings.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.view_user_drawings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userDrawings.setVisibility(View.VISIBLE);
                userPictures.setVisibility(View.GONE);
            }
        });

        showStartup();

        /*
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://52.4.247.20:8081/user/1";


        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.e("HTTP",response);
                        username.setText(response.substring(0,100));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                username.setText("That didn't work!");
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

         */
        /*
        String url ="http://52.4.247.20:8081/user/"+String.valueOf(userId);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            username.setText(response.get("pseudo").toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        */



    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putInt("userId", userId );
        if (origin.equals("myself")) {
            bundle.putInt("visitUserId", userId );
        } else {
            bundle.putInt("visitUserId", visitUserId );
        }

        UserPicturesListFragment fragmentPictures = new UserPicturesListFragment();
        fragmentPictures.setArguments(bundle);
        transaction.add(R.id.fragment_container_user_pictures,fragmentPictures);

        UserDrawingsListFragment fragmentDrawings = new UserDrawingsListFragment();
        fragmentDrawings.setArguments(bundle);
        transaction.add(R.id.fragment_container_user_drawings,fragmentDrawings);

        transaction.commit();
    }


    public void downloadProfilePicture(String url) {

        // Retrieves an image specified by the URL, displays it in the UI.
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        profilePicture.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // ERROR
                    }
                });
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(request);
    }


    public void getUserInfo (String userId) {

        String url = "http://52.4.247.20:8081/user/"+userId;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //editUsername.setText(response.get("pseudo").toString());
                            //editEmail.setText(response.get("email").toString());
                            //editPassword.setText(response.get("password").toString());
                            profilePictureUrl = response.get("image").toString();
                            downloadProfilePicture(profilePictureUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }


    public void putUserEdit () {

        String url = "http://52.4.247.20:8081/user/"+userId;

        String jsonStr = "{\"pseudo\":\""+editUsername.getText()+"\",\"email\":\""+editEmail.getText()+"\",\"password\":\""+editPassword.getText()+"\"}";
        JSONObject json = null;
        Log.e("PUT",jsonStr);

        Context c = this;

        try {
            json = new JSONObject(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try{
                            userPseudo = response.get("pseudo").toString();
                            username.setText(response.get("pseudo").toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.e("PUT", "OK");
                        Toast.makeText(c, "User updated !", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("PUT", error.toString());
                    }
                });



        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }


}