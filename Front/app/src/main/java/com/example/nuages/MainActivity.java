package com.example.nuages;

import static java.security.Security.getProviders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.nuages.databinding.ActivityMainBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    private int userId;
    private String userPseudo;
    private Bitmap weatherIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.title_fragment);

        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = (int) extras.get("userId");
            userPseudo = (String) extras.get("userPseudo");
            weatherIcon = (Bitmap) extras.get("weatherIcon");
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        //askPermissions();
        //getCoord();
        showStartup();
    }




    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putInt("userId", userId );
        bundle.putString("userPseudo",userPseudo);
        bundle.putParcelable("weatherIcon",weatherIcon);

        HomeListFragment fragment = new HomeListFragment();
        fragment.setArguments(bundle);
        transaction.replace(R.id.fragment_container_home,fragment);

        ControlsFragment fragment2 = new ControlsFragment();
        fragment2.setArguments(bundle);
        transaction.add(R.id.fragment_container_controls,fragment2);

        TitleFragment fragment3 = new TitleFragment();
        fragment3.setArguments(bundle);
        transaction.add(R.id.fragment_container_title,fragment3);


        transaction.commit();
    }




}