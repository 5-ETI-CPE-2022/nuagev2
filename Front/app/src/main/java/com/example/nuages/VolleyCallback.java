package com.example.nuages;

import org.json.JSONException;

interface VolleyCallBack {
    void onSuccess() throws JSONException;
}