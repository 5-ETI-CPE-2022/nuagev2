package com.example.nuages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

public class CommentsActivity extends AppCompatActivity {

    private int userId;
    private String userPseudo;
    private Bitmap weatherIcon;

    private String photoId;
    private String photoPath;

    private ImageButton goback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = (int) extras.get("userId");
            userPseudo = (String) extras.get(("userPseudo"));
            photoId = (String) extras.get("photoId");
            photoPath = (String) extras.get("photoPath");
            weatherIcon = (Bitmap) extras.get("weatherIcon");
        }


        goback = findViewById(R.id.comments_to_main);

        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        showStartup();

    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putInt("userId", userId );
        bundle.putString("userPseudo",userPseudo);
        bundle.putParcelable("weatherIcon",weatherIcon);


        TitleFragment fragment3 = new TitleFragment();
        fragment3.setArguments(bundle);
        transaction.add(R.id.fragment_container_title_comments,fragment3);

        bundle.putString("photoId",photoId);
        bundle.putString("photoPath",photoPath);

        CommentsListFragment fragment = new CommentsListFragment();
        fragment.setArguments(bundle);
        transaction.replace(R.id.fragment_container_home_comments,fragment);


        transaction.commit();
    }

}