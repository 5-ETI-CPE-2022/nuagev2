package com.example.nuages;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.nuages.databinding.HomeItemBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class HomeListAdapter extends
        RecyclerView.Adapter<HomeListAdapter.ViewHolder> {

    private int userId;
    private String userPseudo;
    private Bitmap weatherIcon;

    private int nbLikes;

    List<CloudPicture> cloudPictureList;

    public HomeListAdapter(List<CloudPicture> fileList, int id, String pseudo, Bitmap weatherIcon) {
        userId = id;
        userPseudo = pseudo;
        this.weatherIcon = weatherIcon;
        assert fileList != null;
        cloudPictureList = fileList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        HomeItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.home_item, parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CloudPicture file = cloudPictureList.get(position);
        holder.binding.homeItemPhotographer.setText(file.getPhotographer());
        Context c = holder.itemView.getContext();
        showPicture(file.getFilePath(), holder.binding.homeItemPicture, c);
        showPicture(file.getDrawing(), holder.binding.homeItemDrawing, c);
        //Drawable picture = c.getResources().getDrawable(c.getResources().getIdentifier(file.getFilePath(), "drawable", c.getPackageName()));
        //holder.binding.homeItemPicture.setBackground(picture);
        //Drawable drawing = c.getResources().getDrawable(c.getResources().getIdentifier(file.getDrawing(), "drawable", c.getPackageName()));
        //holder.binding.homeItemDrawing.setBackground(drawing);

        holder.binding.homeItemTag.setText(file.getTag());

        holder.binding.homeItemCloudyIdea.setText(file.getTagCloudy()+" ?");

        nbLikes = file.getListLikes().size();
        holder.binding.homeItemNbLikes.setText(String.valueOf(nbLikes));

        if (file.getListLikes().contains(userId)) {
            holder.binding.homeItemStar.setChecked(true);
        }

        holder.binding.homeItemStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.binding.homeItemStar.isChecked()) {
                    addRemoveLike("add", file.getIdDrawing(), holder.itemView.getContext());
                    holder.binding.homeItemNbLikes.setText(String.valueOf(Integer.parseInt(holder.binding.homeItemNbLikes.getText().toString())+1));
                } else {
                    addRemoveLike("remove", file.getIdDrawing(), holder.itemView.getContext());
                    holder.binding.homeItemNbLikes.setText(String.valueOf(Integer.parseInt(holder.binding.homeItemNbLikes.getText().toString())-1));
                }
            }
        });

        holder.binding.homeItemSeeDrawing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView imgView = (ImageView) holder.binding.homeItemDrawing;
                if (imgView.getVisibility() == View.VISIBLE) {
                    imgView.setVisibility(View.INVISIBLE);
                    holder.binding.homeItemTag.setVisibility(View.INVISIBLE);
                } else {
                    imgView.setVisibility(View.VISIBLE);
                    holder.binding.homeItemTag.setVisibility(View.VISIBLE);
                }
            }
        });

        holder.binding.homeItemDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context c = holder.itemView.getContext();
                Intent pictureEditorActivityIntent = new Intent(c, PictureEditorActivity.class);
                Uri uri = Uri.parse("android.resource://com.example.nuages/drawable/nuage");
                pictureEditorActivityIntent.putExtra("uri",uri);
                pictureEditorActivityIntent.putExtra("pictureId", Integer.valueOf(file.getId()));
                pictureEditorActivityIntent.putExtra("picture_path",file.getFilePath());
                pictureEditorActivityIntent.putExtra("userId", userId);
                pictureEditorActivityIntent.putExtra("userPseudo", userPseudo);
                pictureEditorActivityIntent.putExtra("origin", "other");
                pictureEditorActivityIntent.putExtra("weatherIcon", weatherIcon);
                c.startActivity(pictureEditorActivityIntent);
            }
        });

        holder.binding.homeItemComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context c = holder.itemView.getContext();
                Intent commentsActivityIntent = new Intent(c, CommentsActivity.class);
                commentsActivityIntent.putExtra("userId", userId);
                commentsActivityIntent.putExtra("userPseudo", userPseudo);
                commentsActivityIntent.putExtra("photoId", file.getId());
                commentsActivityIntent.putExtra("photoPath", file.getFilePath());
                commentsActivityIntent.putExtra("weatherIcon", weatherIcon);
                c.startActivity(commentsActivityIntent);
            }
        });

        holder.binding.homeItemPhotographer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context c = holder.itemView.getContext();
                Intent visitUserActivityIntent = new Intent(c, UserActivity.class);
                int idPhotographer = file.getIdPhotographer();
                if (idPhotographer == userId) {
                    visitUserActivityIntent.putExtra("userId", userId);
                    visitUserActivityIntent.putExtra("userPseudo", userPseudo);
                    visitUserActivityIntent.putExtra("origin","myself");
                    visitUserActivityIntent.putExtra("weatherIcon", weatherIcon);
                } else {
                    visitUserActivityIntent.putExtra("userId", userId);
                    visitUserActivityIntent.putExtra("userPseudo", userPseudo);
                    visitUserActivityIntent.putExtra("visitUserId", file.getIdPhotographer());
                    visitUserActivityIntent.putExtra("visitUserPseudo", file.getPhotographer());
                    visitUserActivityIntent.putExtra("origin","visit");
                    visitUserActivityIntent.putExtra("weatherIcon", weatherIcon);
                }
                c.startActivity(visitUserActivityIntent);
            }
        });

        holder.binding.homeItemHideButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.binding.homeItemButtons.getVisibility() == View.INVISIBLE) {
                    holder.binding.homeItemButtons.setVisibility(View.VISIBLE);
                } else if (holder.binding.homeItemButtons.getVisibility() == View.VISIBLE) {
                    holder.binding.homeItemButtons.setVisibility(View.INVISIBLE);
                }

            }
        });

        holder.binding.homeItemDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context c = holder.itemView.getContext();

                ImageView picture = holder.binding.homeItemPicture;
                picture.setDrawingCacheEnabled(true);
                String pictureSaved = MediaStore.Images.Media.insertImage(
                        c.getContentResolver(), picture.getDrawingCache(), UUID.randomUUID()
                                .toString(), "picture");
                picture.destroyDrawingCache();

                ImageView drawing = holder.binding.homeItemDrawing;
                drawing.setDrawingCacheEnabled(true);
                String drawingSaved = MediaStore.Images.Media.insertImage(
                        c.getContentResolver(), drawing.getDrawingCache(), UUID.randomUUID()
                                .toString(), "drawing");
                picture.destroyDrawingCache();

                Toast.makeText(c, "Picture downloaded !", Toast.LENGTH_SHORT).show();
            }
        });

        holder.binding.homeItemShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imageView = holder.binding.homeItemDrawing;
                Drawable drawable = imageView.getDrawable();
                Bitmap bmp = null;
                if (drawable instanceof BitmapDrawable){
                    bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                }
                // Store image to default external storage directory
                Uri bmpUri = null;
                try {
                    File file =  new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".jpeg");
                    file.getParentFile().mkdirs();
                    FileOutputStream out = new FileOutputStream(file);
                    bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.close();
                    bmpUri = FileProvider.getUriForFile(c, c.getApplicationContext().getPackageName() + ".provider", file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                shareIntent.setType("image/*");
                // Launch sharing dialog for image
                c.startActivity(Intent.createChooser(shareIntent, "Share Image"));
            }
        });


        holder.binding.homeItemCloudy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.binding.homeItemCloudySpeak.getVisibility() == View.GONE) {
                    holder.binding.homeItemCloudySpeak.setVisibility(View.VISIBLE);
                } else if (holder.binding.homeItemCloudySpeak.getVisibility() == View.VISIBLE) {
                    holder.binding.homeItemCloudySpeak.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return cloudPictureList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private HomeItemBinding binding;
        //private CloudPictureViewModel viewModel = new CloudPictureViewModel();
        ViewHolder(HomeItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            //this.binding.setCloudPictureViewModel(viewModel);
        }
    }


    public void showPicture(String url, ImageView imageView, Context c) {

        // Retrieves an image specified by the URL, displays it in the UI.
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imageView.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // ERROR
                    }
                });
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(c).addToRequestQueue(request);
    }

    public void addRemoveLike (String addRemove, String idDrawing, Context c) {

        String url = "http://52.4.247.20:8083/storage/"+addRemove+"Like/user/"+userId+"/dessin/"+idDrawing;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(c).addToRequestQueue(jsonObjectRequest);

    }

}
