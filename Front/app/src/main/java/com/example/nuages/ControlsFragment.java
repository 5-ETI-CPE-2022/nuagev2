package com.example.nuages;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.nuages.databinding.ControlsFragmentBinding;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class ControlsFragment extends Fragment {

    ImageButton buttonHome;
    ImageButton buttonCamera;
    ImageButton buttonUser;

    private Uri mImageUri;

    private int userId;
    private String userPseudo;
    private Bitmap weatherIcon;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        ControlsFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.controls_fragment,container,false);

        userId = this.getArguments().getInt("userId");
        userPseudo = this.getArguments().getString("userPseudo");

        weatherIcon = this.getArguments().getParcelable("weatherIcon");

        buttonHome = binding.buttonHome;
        buttonCamera = binding.buttonCamera;
        buttonUser = binding.buttonUser;

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivityIntent = new Intent(getActivity(), MainActivity.class);
                getActivity().finish();
                mainActivityIntent.putExtra("userId", userId);
                mainActivityIntent.putExtra("userPseudo", userPseudo);
                mainActivityIntent.putExtra("weatherIcon", weatherIcon);
                getActivity().overridePendingTransition(0, 0);
                startActivity(mainActivityIntent);
                getActivity().overridePendingTransition(0, 0);
            }
        });

        buttonUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent userActivityIntent = new Intent(getActivity(), UserActivity.class);
                userActivityIntent.putExtra("userId", userId);
                userActivityIntent.putExtra("userPseudo", userPseudo);
                userActivityIntent.putExtra("weatherIcon", weatherIcon);
                userActivityIntent.putExtra("origin","myself");
                getActivity().overridePendingTransition(0, 0);
                startActivity(userActivityIntent);
                getActivity().overridePendingTransition(0, 0);
            }
        });

        buttonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Open Camera
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


                File photo;
                try
                {
                    // place where to store camera taken picture
                    photo = createTemporaryFile("picture", ".jpg");
                    photo.delete();
                }
                catch(Exception e)
                {
                    Toast.makeText(getActivity().getApplicationContext(), "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT).show();
                    return;
                }

                Log.d("FILE", photo.getAbsolutePath());

                //mImageUri = Uri.fromFile(photo);
                //Uri mImageUri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", photo);
                mImageUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider", photo);

                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                // Probleme possible si rotation appareil photo
                startActivityForResult(intent, 100);
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 100) {
            if(resultCode == getActivity().RESULT_OK) {
                // Get Capture Image
                //Bitmap captureImage = (Bitmap) data.getExtras().get("data");
                Bitmap captureImage = grabImage();
                // Post Image
                ///////////postPictureBackend(captureImage);
                Intent pictureEditorActivityIntent = new Intent(getActivity(), PictureEditorActivity.class);
                //pictureEditorActivityIntent.putExtra("image",captureImage);
                pictureEditorActivityIntent.putExtra("uri", mImageUri);
                pictureEditorActivityIntent.putExtra("userId", userId);
                pictureEditorActivityIntent.putExtra("userPseudo", userPseudo);
                pictureEditorActivityIntent.putExtra("origin", "myself");
                pictureEditorActivityIntent.putExtra("weatherIcon", weatherIcon);
                startActivity(pictureEditorActivityIntent);
            }
            // RESULT_CANCELED
        }
    }


    private File createTemporaryFile(String part, String ext) throws IOException {
        File tempDir= Environment.getExternalStorageDirectory();
        tempDir=new File(tempDir.getAbsolutePath()+"/.temp/");
        if(!tempDir.exists())
        {
            tempDir.mkdirs();
        }
        // /storage/emulated/O/.temp
        Log.d("TEST", tempDir.getAbsolutePath());
        // File file = File.createTempFile(part, ext);
        // Log.d("FILE", file.getAbsolutePath());
        //return File.createTempFile(part, ext, tempDir);
        return File.createTempFile(part, ext);
    }

    public Bitmap grabImage()
    {
        //getActivity().getContentResolver().notifyChange(mImageUri, null);
        ContentResolver cr = getActivity().getContentResolver();
        Bitmap bitmap;
        try
        {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
            return bitmap;
        }
        catch (Exception e)
        {
            Toast.makeText(getActivity().getApplicationContext(), "Failed to load", Toast.LENGTH_SHORT).show();
            return null;
        }
    }


    public void postPictureBackend(Bitmap captureImage) throws IOException {

        // Static stuff
        String attachmentName = "bitmap";
        String attachmentFileName = "bitmap.bmp";
        String crlf = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";


        // Setup Request
        URL url = new URL("localhost/uploadImage");
        HttpURLConnection c;
        c = (HttpURLConnection) url.openConnection();
        c.setUseCaches(false);
        c.setDoOutput(true);

        c.setRequestMethod("POST");
        c.setRequestProperty("Connection", "Keep-Alive");
        c.setRequestProperty("Cache-Control", "no-cache");
        c.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

        // Start Content Wrapper
        DataOutputStream request = new DataOutputStream(c.getOutputStream());

        request.writeBytes(twoHyphens + boundary + crlf);
        request.writeBytes("Content-Disposition: form-data; name=\"" +
                attachmentName + "\";filename=\"" +
                attachmentFileName + "\"" + crlf);
        request.writeBytes(crlf);

        // Convert Bitmap to Byte Buffer
        //I want to send only 8 bit black & white bitmaps
        byte[] pixels = new byte[captureImage.getWidth() * captureImage.getHeight()];
        for (int i = 0; i < captureImage.getWidth(); ++i) {
            for (int j = 0; j < captureImage.getHeight(); ++j) {
                //we're interested only in the MSB of the first byte,
                //since the other 3 bytes are identical for B&W images
                pixels[i + j] = (byte) ((captureImage.getPixel(i, j) & 0x80) >> 7);
            }
        }

        request.write(pixels);

        // End Content Wrapper
        request.writeBytes(crlf);
        request.writeBytes(twoHyphens + boundary + twoHyphens + crlf);

        // Flush Output Buffer
        request.flush();
        request.close();

        // Get Response
        InputStream responseStream = new
                BufferedInputStream(c.getInputStream());

        BufferedReader responseStreamReader =
                new BufferedReader(new InputStreamReader(responseStream));

        String line = "";
        StringBuilder stringBuilder = new StringBuilder();

        while ((line = responseStreamReader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }
        responseStreamReader.close();

        String response = stringBuilder.toString();

        // Close Response Stream
        responseStream.close();

        // Close the connection
        c.disconnect();

    }





    /**
     * This method is responsible for solving the rotation issue if exist. Also scale the images to
     * 1024x1024 resolution
     *
     * @param context       The current context
     * @param selectedImage The Image URI
     * @return Bitmap image results
     * @throws IOException
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    /**
     * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
     * the closest inSampleSize that will result in the final decoded bitmap having a width and
     * height equal to or larger than the requested width and height. This implementation does not
     * ensure a power of 2 is returned for inSampleSize which can be faster when decoding but
     * results in a larger bitmap which isn't as useful for caching purposes.
     *
     * @param options   An options object with out* params already populated (run through a decode*
     *                  method with inJustDecodeBounds==true
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

}
