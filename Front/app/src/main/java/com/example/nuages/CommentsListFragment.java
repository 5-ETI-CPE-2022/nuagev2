package com.example.nuages;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.nuages.databinding.CommentsListFragmentBinding;
import com.example.nuages.databinding.HomeListFragmentBinding;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class CommentsListFragment extends Fragment {

    private int userId;
    private String userPseudo;
    private Bitmap weatherIcon;

    private String photoId;
    private String photoPath;
    private Bitmap photo;

    public List<String> filePathList = new ArrayList<String>();
    public List<String> drawingList = new ArrayList<String>();
    public List<String> photographerList = new ArrayList<String>();
    public List<String> idList = new ArrayList<String>();
    public List<ArrayList<Integer>> listLikesList = new ArrayList<>();
    public List<String> tagList = new ArrayList<>();



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        CommentsListFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.comments_list_fragment,container,false);
        binding.commentsList.setLayoutManager(new LinearLayoutManager(
                binding.getRoot().getContext()));

        userId = this.getArguments().getInt("userId");
        userPseudo = this.getArguments().getString("userPseudo");
        photoId = this.getArguments().getString("photoId");
        photoPath = this.getArguments().getString("photoPath");

        weatherIcon = this.getArguments().getParcelable("weatherIcon");

        getPhoto(new VolleyCallBack() {
            @Override
            public void onSuccess() throws JSONException {


                getComments(photoId, new VolleyCallBack() {
                    @Override
                    public void onSuccess() throws JSONException {
                        // this is where you will call the geofire, here you have the response from the volley.

                        //Log.e("PICTURES",filePathList.toString());
                        //Log.e("DRAWINGS",drawingList.toString());
                        //Log.e("PHOTOGRAPHERS",photographerList.toString());

                        initList(binding);

                    }

                });


            }
        });

        binding.commentsList.setAdapter(new CommentsListAdapter(new ArrayList<>(), userId, userPseudo, photoId, photo, weatherIcon));
        return binding.getRoot();
    }

    void initList(CommentsListFragmentBinding binding) throws JSONException {
        List<CloudPicture> fakeList = new ArrayList<>();
        for (int i = 0; i < drawingList.size(); i++) {
            CloudPicture cloudPicture = new CloudPicture();
            cloudPicture.setFilePath(photoPath);
            cloudPicture.setDrawing(drawingList.get(i));
            cloudPicture.setPhotographer(photographerList.get(i));
            cloudPicture.setId(idList.get(i));
            cloudPicture.setListLikes(listLikesList.get(i));
            cloudPicture.setTag(tagList.get(i));

            fakeList.add(cloudPicture);
        }

        binding.commentsList.setAdapter(new CommentsListAdapter(fakeList, userId, userPseudo, photoId, photo, weatherIcon));
    }


    public void getComments(String photoId, final VolleyCallBack callBack) {


        String url ="http://52.4.247.20:8083/storage/getComments/image/" + photoId;


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        for(int i=0;i<response.length();i++) {

                            try {
                                //filePathList.add("https://"+photoPath);
                                Log.e("DESSIN",response.getJSONObject(i).get("url").toString());
                                photographerList.add(response.getJSONObject(i).get("user_name").toString());
                                drawingList.add("https://"+response.getJSONObject(i).get("url").toString());
                                idList.add(response.getJSONObject(i).get("id").toString());

                                JSONArray jsonArray = (JSONArray) response.getJSONObject(i).get("likes");
                                ArrayList<Integer> listdata = new ArrayList<>();
                                for (int j=0;j<jsonArray.length();j++){
                                    listdata.add(Integer.valueOf(jsonArray.getString(j)));
                                }

                                listLikesList.add(listdata);

                                String tag = response.getJSONObject(i).get("tag").toString();
                                tagList.add(tag);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        try {
                            callBack.onSuccess();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("PICTURES","ERREUR");
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(getActivity().getApplicationContext()).addToRequestQueue(jsonArrayRequest);
    }


    public void getPhoto(final VolleyCallBack callBack) {

        // Retrieves an image specified by the URL, displays it in the UI.
        ImageRequest request = new ImageRequest(photoPath,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        photo = bitmap;

                        try {
                            callBack.onSuccess();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }



                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // ERROR
                    }
                });
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(getActivity().getApplicationContext()).addToRequestQueue(request);
    }


}
