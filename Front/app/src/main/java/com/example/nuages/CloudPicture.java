package com.example.nuages;

import java.util.ArrayList;

public class CloudPicture {

    private String id;
    private String idDrawing;
    private String filePath;
    private String drawing;
    private String photographer;
    private int idPhotographer;
    private ArrayList<Integer> listLikes;
    private String tagCloudy;
    private String tag;
    //private String location;
    //private int year;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdDrawing() {
        return idDrawing;
    }

    public void setIdDrawing(String idDrawing) {
        this.idDrawing = idDrawing;
    }

    public String getPhotographer() {
        return photographer;
    }

    public void setPhotographer(String photographer) {
        this.photographer = photographer;
    }

    public int getIdPhotographer() {
        return idPhotographer;
    }

    public void setIdPhotographer(int idPhotographer) {
        this.idPhotographer = idPhotographer;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getDrawing() {
        return drawing;
    }

    public void setDrawing(String drawing) {
        this.drawing = drawing;
    }

    public ArrayList<Integer> getListLikes() {return listLikes; }

    public void setListLikes(ArrayList<Integer> listLikes) { this.listLikes = listLikes;}

    public String getTagCloudy() { return tagCloudy;}

    public void setTagCloudy(String tagCloudy) {this.tagCloudy = tagCloudy;}

    public String getTag() { return tag;}

    public void setTag(String tag) {this.tag = tag;}

    /*
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    */
}
