package com.example.nuages;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.nuages.databinding.UserDrawingsItemBinding;
import com.example.nuages.databinding.UserPicturesItemBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class UserDrawingsListAdapter extends
        RecyclerView.Adapter<UserDrawingsListAdapter.ViewHolder>{


    private int userId;
    private int visitUserId;
    private String photoId;

    private String photoPath;
    private Bitmap photo;

    List<CloudPicture> cloudPictureList;

    public UserDrawingsListAdapter(List<CloudPicture> fileList, int id, int visitId, String photoId) {
        userId = id;
        visitUserId = visitId;
        this.photoId = photoId;
        assert fileList != null;
        cloudPictureList = fileList;
    }

    @NonNull
    @Override
    public UserDrawingsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        UserDrawingsItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.user_drawings_item, parent,false);
        return new UserDrawingsListAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserDrawingsListAdapter.ViewHolder holder, int position) {
        CloudPicture file = cloudPictureList.get(position);
        Context c = holder.itemView.getContext();
        showPicture(file.getFilePath(), holder.binding.userItemDrawing, c);


        if (userId == visitUserId) {

            holder.binding.userItemDrawing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context c = holder.itemView.getContext();

                    updateProfilePicture(file.getFilePath(), c);


                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return cloudPictureList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private UserDrawingsItemBinding binding;
        //private CloudPictureViewModel viewModel = new CloudPictureViewModel();
        ViewHolder(UserDrawingsItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            //this.binding.setCloudPictureViewModel(viewModel);
        }
    }


    public void showPicture(String url, ImageView imageView, Context c) {

        // Retrieves an image specified by the URL, displays it in the UI.
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imageView.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // ERROR
                    }
                });
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(c).addToRequestQueue(request);
    }


    public void updateProfilePicture (String urlPicture, Context c) {

        String url = "http://52.4.247.20:8081/user/"+userId;

        String jsonStr = "{\"image\":\""+urlPicture+"\"}";
        JSONObject json = null;
        try {
            json = new JSONObject(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(c, "Profile picture updated !", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });



        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(c).addToRequestQueue(jsonObjectRequest);

    }


}
