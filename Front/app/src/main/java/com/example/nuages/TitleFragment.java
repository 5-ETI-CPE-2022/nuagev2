package com.example.nuages;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.nuages.databinding.ControlsFragmentBinding;
import com.example.nuages.databinding.TitleFragmentBinding;

import org.json.JSONException;
import org.json.JSONObject;

public class TitleFragment extends Fragment {

    ImageView weatherImageView;
    Bitmap weatherIcon;

    //Location location;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        TitleFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.title_fragment,container,false);

        weatherImageView = binding.weatherIconTitle;

        weatherIcon = this.getArguments().getParcelable("weatherIcon");

        weatherImageView.setImageBitmap(weatherIcon);


        //getCoord();
        //Log.e("GPS", "Latitude " + location.getLatitude() + " et longitude " + location.getLongitude());
        //get_meteo(Double.toString(location.getLatitude()), Double.toString(location.getLongitude()));


        return binding.getRoot();
    }

    // Couleur jaune #FFE65E


    /*
        API Météo
     */
    /*
    public void getCoord() {
        Log.e("WEATHER","Coord");
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        Criteria critere = new Criteria();

        // Pour indiquer la précision voulue
        // On peut mettre ACCURACY_FINE pour une haute précision ou ACCURACY_COARSE pour une moins bonne précision
        critere.setAccuracy(Criteria.ACCURACY_FINE);

        // Est-ce que le fournisseur doit être capable de donner une altitude ?
        critere.setAltitudeRequired(false);

        // Est-ce que le fournisseur doit être capable de donner une direction ?
        critere.setBearingRequired(false);

        // Est-ce que le fournisseur peut être payant ?
        critere.setCostAllowed(false);

        // Pour indiquer la consommation d'énergie demandée
        // Criteria.POWER_HIGH pour une haute consommation, Criteria.POWER_MEDIUM pour une consommation moyenne et Criteria.POWER_LOW pour une basse consommation
        critere.setPowerRequirement(Criteria.POWER_LOW);

        // Est-ce que le fournisseur doit être capable de donner une vitesse ?
        critere.setSpeedRequired(false);

        String provider = locationManager.getBestProvider(critere, true);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        location = locationManager.getLastKnownLocation(provider);
        //Location location = locationManager.getLastKnownLocation(provider);
        //Log.d("myTag", location.toString());

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 150, new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }

            @Override
            public void onLocationChanged(Location location) {
                //Log.d("GPS", "Latitude " + location.getLatitude() + " et longitude " + location.getLongitude());
                //get_meteo(Double.toString(location.getLatitude()), Double.toString(location.getLongitude()));
            }
        });
    }

    public void get_meteo(String latitude, String longitude) {
        Log.e("WEATHER","Meteo");
        String url ="http://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon=" + longitude + "&appid=04886a9b8c45e8e99d5dcb7224db7416";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("REPONSE METEO",response.toString());
                        String id_icone = "";
                        try {
                            id_icone = response.getJSONArray("weather").getJSONObject(0).getString("icon");
                            Log.d("ID ICONE",id_icone);
                            downloadIconeMeteo(id_icone);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

    }


    public void downloadIconeMeteo(String id) {
        Log.e("WEATHER","Icon");
        String url = "http://openweathermap.org/img/wn/" + id + "@2x.png";
        Log.d("URL IMAGE",url);
        ImageRequest request=new ImageRequest (url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap bitmap) {
                weatherIcon.setImageBitmap(bitmap);
                Log.d("PAS D ERREUR","");

            }
        },0,0, ImageView.ScaleType.CENTER_CROP,null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ERREUR","");
            }});


        // Add the request to the RequestQueue.
        MySingleton.getInstance(getActivity()).addToRequestQueue(request);
        Log.d("LA REQUETE",request.toString());
    }
    */

}
