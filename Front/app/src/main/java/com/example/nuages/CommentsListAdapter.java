package com.example.nuages;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.nuages.databinding.CommentsItemBinding;
import com.example.nuages.databinding.HomeItemBinding;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class CommentsListAdapter extends
        RecyclerView.Adapter<CommentsListAdapter.ViewHolder> {

    private int userId;
    private String userPseudo;
    private String photoId;
    private Bitmap weatherIcon;

    private int nbLikes;

    private String photoPath;
    private Bitmap photo;

    List<CloudPicture> cloudPictureList;

    public CommentsListAdapter(List<CloudPicture> fileList, int id, String pseudo, String photoId, Bitmap photo, Bitmap weatherIcon) {
        userId = id;
        userPseudo = pseudo;
        this.photo = photo;
        this.weatherIcon = weatherIcon;
        assert fileList != null;
        cloudPictureList = fileList;
    }

    @NonNull
    @Override
    public CommentsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CommentsItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.comments_item, parent,false);
        return new CommentsListAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsListAdapter.ViewHolder holder, int position) {
        CloudPicture file = cloudPictureList.get(position);
        holder.binding.commentsItemPhotographer.setText(file.getPhotographer());
        Context c = holder.itemView.getContext();
        showPicture(file.getDrawing(), holder.binding.commentsItemDrawing, c);
        holder.binding.commentsItemPicture.setImageBitmap(photo);
        //showPicture(file.getFilePath(), holder.binding.commentsItemPicture, c);

        //Drawable picture = c.getResources().getDrawable(c.getResources().getIdentifier(file.getFilePath(), "drawable", c.getPackageName()));
        //holder.binding.homeItemPicture.setBackground(picture);
        //Drawable drawing = c.getResources().getDrawable(c.getResources().getIdentifier(file.getDrawing(), "drawable", c.getPackageName()));
        //holder.binding.homeItemDrawing.setBackground(drawing);

        holder.binding.commentsItemTag.setText(file.getTag());

        nbLikes = file.getListLikes().size();
        holder.binding.commentsItemNbLikes.setText(String.valueOf(nbLikes));

        if (file.getListLikes().contains(userId)) {
            holder.binding.commentsItemStar.setChecked(true);
        }

        holder.binding.commentsItemStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.binding.commentsItemStar.isChecked()) {
                    addRemoveLike("add", file.getId(), holder.itemView.getContext());
                    holder.binding.commentsItemNbLikes.setText(String.valueOf(Integer.parseInt(holder.binding.commentsItemNbLikes.getText().toString())+1));
                } else {
                    addRemoveLike("remove", file.getId(), holder.itemView.getContext());
                    holder.binding.commentsItemNbLikes.setText(String.valueOf(Integer.parseInt(holder.binding.commentsItemNbLikes.getText().toString())-1));
                }
            }
        });

        holder.binding.commentsItemSeeDrawing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView imgView = (ImageView) holder.binding.commentsItemDrawing;
                if (imgView.getVisibility() == View.VISIBLE) {
                    imgView.setVisibility(View.INVISIBLE);
                    holder.binding.commentsItemTag.setVisibility(View.INVISIBLE);
                } else {
                    imgView.setVisibility(View.VISIBLE);
                    holder.binding.commentsItemTag.setVisibility(View.VISIBLE);
                }
            }
        });

        holder.binding.commentsItemPhotographer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context c = holder.itemView.getContext();
                Intent visitUserActivityIntent = new Intent(c, UserActivity.class);
                int idPhotographer = file.getIdPhotographer();
                if (file.getPhotographer().equals(userPseudo)) {
                    visitUserActivityIntent.putExtra("userId", userId);
                    visitUserActivityIntent.putExtra("userPseudo", userPseudo);
                    visitUserActivityIntent.putExtra("origin","myself");
                    visitUserActivityIntent.putExtra("weatherIcon", weatherIcon);
                } else {
                    visitUserActivityIntent.putExtra("userId", userId);
                    visitUserActivityIntent.putExtra("userPseudo", userPseudo);
                    visitUserActivityIntent.putExtra("visitUserId", file.getIdPhotographer());
                    visitUserActivityIntent.putExtra("visitUserPseudo", file.getPhotographer());
                    visitUserActivityIntent.putExtra("origin","visit");
                    visitUserActivityIntent.putExtra("weatherIcon", weatherIcon);
                }
                c.startActivity(visitUserActivityIntent);
            }
        });

        holder.binding.commentsItemHideButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.binding.commentsItemButtons.getVisibility() == View.INVISIBLE) {
                    holder.binding.commentsItemButtons.setVisibility(View.VISIBLE);
                } else if (holder.binding.commentsItemButtons.getVisibility() == View.VISIBLE) {
                    holder.binding.commentsItemButtons.setVisibility(View.INVISIBLE);
                }

            }
        });

        holder.binding.commentsItemDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context c = holder.itemView.getContext();

                ImageView picture = holder.binding.commentsItemPicture;
                picture.setDrawingCacheEnabled(true);
                String pictureSaved = MediaStore.Images.Media.insertImage(
                        c.getContentResolver(), picture.getDrawingCache(), UUID.randomUUID()
                                .toString(), "picture");
                picture.destroyDrawingCache();

                ImageView drawing = holder.binding.commentsItemDrawing;
                drawing.setDrawingCacheEnabled(true);
                String drawingSaved = MediaStore.Images.Media.insertImage(
                        c.getContentResolver(), drawing.getDrawingCache(), UUID.randomUUID()
                                .toString(), "drawing");
                picture.destroyDrawingCache();

                Toast.makeText(c, "Picture downloaded !", Toast.LENGTH_SHORT).show();
            }
        });

        holder.binding.commentsItemShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImageView imageView = holder.binding.commentsItemPicture;
                Drawable drawable = imageView.getDrawable();
                Bitmap bmp = null;
                if (drawable instanceof BitmapDrawable){
                    bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                }
                // Store image to default external storage directory
                Uri bmpUri = null;
                try {
                    File file =  new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS), "share_picture_" + System.currentTimeMillis() + ".jpeg");
                    file.getParentFile().mkdirs();
                    FileOutputStream out = new FileOutputStream(file);
                    bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.close();
                    bmpUri = FileProvider.getUriForFile(c, c.getApplicationContext().getPackageName() + ".provider", file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ImageView imageView2 = holder.binding.commentsItemDrawing;
                Drawable drawable2 = imageView2.getDrawable();
                Bitmap bmp2 = null;
                if (drawable instanceof BitmapDrawable){
                    bmp2 = ((BitmapDrawable) imageView2.getDrawable()).getBitmap();
                }
                // Store image to default external storage directory
                Uri bmpUri2 = null;
                try {
                    File file2 =  new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS), "share_drawing_" + System.currentTimeMillis() + ".jpeg");
                    file2.getParentFile().mkdirs();
                    FileOutputStream out2 = new FileOutputStream(file2);
                    bmp2.compress(Bitmap.CompressFormat.JPEG, 90, out2);
                    out2.close();
                    bmpUri2 = FileProvider.getUriForFile(c, c.getApplicationContext().getPackageName() + ".provider", file2);
                } catch (IOException e) {
                    e.printStackTrace();
                }



                Intent shareIntent = new Intent();
                //shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
                shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri2);
                shareIntent.setType("image/*");
                // Launch sharing dialog for image
                c.startActivity(Intent.createChooser(shareIntent, "Share Image"));
            }
        });

    }

    @Override
    public int getItemCount() {
        return cloudPictureList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private CommentsItemBinding binding;
        //private CloudPictureViewModel viewModel = new CloudPictureViewModel();
        ViewHolder(CommentsItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            //this.binding.setCloudPictureViewModel(viewModel);
        }
    }


    public void showPicture(String url, ImageView imageView, Context c) {

        // Retrieves an image specified by the URL, displays it in the UI.
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imageView.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // ERROR
                    }
                });
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(c).addToRequestQueue(request);
    }


    public void addRemoveLike (String addRemove, String idDrawing, Context c) {

        String url = "http://52.4.247.20:8083/storage/"+addRemove+"Like/user/"+userId+"/dessin/"+idDrawing;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(c).addToRequestQueue(jsonObjectRequest);

    }

}
