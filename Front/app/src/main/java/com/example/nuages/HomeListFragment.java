package com.example.nuages;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.nuages.databinding.HomeListFragmentBinding;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class HomeListFragment extends Fragment {

    private int userId;
    private String userPseudo;
    private Bitmap weatherIcon;

    public List<String> filePathList = new ArrayList<String>();
    public List<String> drawingList = new ArrayList<String>();
    public List<String> photographerList = new ArrayList<String>();
    public List<String> idList = new ArrayList<String>();
    public List<Integer> idPhotographerList = new ArrayList<Integer>();
    public List<String> idDrawingList = new ArrayList<String>();
    public List<ArrayList<Integer>> listLikesList = new ArrayList<>();
    public List<String> tagCloudyList = new ArrayList<>();
    public List<String> tagList = new ArrayList<>();



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        HomeListFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.home_list_fragment,container,false);
        binding.homeList.setLayoutManager(new LinearLayoutManager(
                binding.getRoot().getContext()));

        userId = this.getArguments().getInt("userId");
        userPseudo = this.getArguments().getString("userPseudo");
        weatherIcon = this.getArguments().getParcelable("weatherIcon");



        getLastPictures(new VolleyCallBack() {
            @Override
            public void onSuccess() throws JSONException {
                // this is where you will call the geofire, here you have the response from the volley.

                //Log.e("PICTURES",filePathList.toString());
                //Log.e("DRAWINGS",drawingList.toString());
                //Log.e("PHOTOGRAPHERS",photographerList.toString());

                initList(binding);

                }

            });

        binding.homeList.setAdapter(new HomeListAdapter(new ArrayList<>(), userId, userPseudo, weatherIcon));
        return binding.getRoot();
    }

    void initList(HomeListFragmentBinding binding) throws JSONException {
        List<CloudPicture> fakeList = new ArrayList<>();
        //ArrayList<String> filePathList = new ArrayList<>(Arrays.asList("nuage", "nuage","nuage","nuage","nuage","nuage","nuage","nuage","nuage","nuage","nuage","nuage"));
        //ArrayList<String> drawingList = new ArrayList<>(Arrays.asList("nuage_drawing", "nuage_drawing", "nuage_drawing", "nuage_drawing", "nuage_drawing", "nuage_drawing", "nuage_drawing", "nuage_drawing", "nuage_drawing", "nuage_drawing", "nuage_drawing", "nuage_drawing"));
        //ArrayList<String> photographerList = new ArrayList<>(Arrays.asList("Pierre", "Marianne", "Alice", "Matthieu", "Pierre", "Marianne", "Alice", "Matthieu", "Pierre", "Marianne", "Alice", "Matthieu"));
        for(int i=0;i<filePathList.size();i++){
            CloudPicture cloudPicture = new CloudPicture();
            cloudPicture.setFilePath(filePathList.get(i));
            cloudPicture.setDrawing(drawingList.get(i));
            cloudPicture.setPhotographer(photographerList.get(i));
            cloudPicture.setId(idList.get(i));
            cloudPicture.setIdPhotographer(idPhotographerList.get(i));
            cloudPicture.setIdDrawing(idDrawingList.get(i));
            cloudPicture.setListLikes(listLikesList.get(i));
            cloudPicture.setTagCloudy(tagCloudyList.get(i));
            cloudPicture.setTag(tagList.get(i));


            fakeList.add(cloudPicture);
        }

        binding.homeList.setAdapter(new HomeListAdapter(fakeList, userId, userPseudo, weatherIcon));
    }


    public void getLastPictures(final VolleyCallBack callBack) {


        String url ="http://52.4.247.20:8082/storage/lastimageswithdessins/50";


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        for(int i=0;i<response.length();i++) {

                            try {
                                filePathList.add("https://"+response.getJSONObject(i).get("url").toString());
                                photographerList.add(response.getJSONObject(i).get("pseudo_user").toString());
                                drawingList.add("https://"+response.getJSONObject(i).getJSONObject("associatedDessin").get("url").toString());
                                idList.add(response.getJSONObject(i).get("id").toString());
                                idPhotographerList.add((Integer) response.getJSONObject(i).getJSONObject("associatedDessin").get("id_user"));
                                idDrawingList.add(response.getJSONObject(i).getJSONObject("associatedDessin").get("id").toString());

                                JSONArray jsonArray = (JSONArray) response.getJSONObject(i).getJSONObject("associatedDessin").get("likes");
                                ArrayList<Integer> listdata = new ArrayList<>();
                                for (int j=0;j<jsonArray.length();j++){
                                    listdata.add(Integer.valueOf(jsonArray.getString(j)));
                                }

                                listLikesList.add(listdata);

                                tagCloudyList.add(response.getJSONObject(i).get("tag").toString());
                                String tag = response.getJSONObject(i).getJSONObject("associatedDessin").get("tag").toString();
                                tagList.add(tag);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        try {
                            callBack.onSuccess();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("PICTURES","ERREUR");
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(getActivity().getApplicationContext()).addToRequestQueue(jsonArrayRequest);
    }


}
