package com.example.nuages;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class CloudPictureViewModel extends BaseObservable {

    private CloudPicture cloudPicture = new CloudPicture();

    public void setCloudPicture(CloudPicture file) {
        cloudPicture = file;
        notifyChange();
    }

    @Bindable
    public String getPhotographer() {
        return cloudPicture.getPhotographer();
    }

    @Bindable
    public String getFilePath() {
        return cloudPicture.getFilePath();
    }

    @Bindable
    public String getDrawing() {
        return cloudPicture.getDrawing();
    }

    /*
    @Bindable
    public String getLocation() {
        return cloudPicture.getLocation();
    }
    */


}
