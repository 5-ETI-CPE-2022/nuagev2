package com.example.nuages;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class SigninActivity extends AppCompatActivity {

    private EditText email;
    private EditText username;
    private EditText password;
    private Button signin;

    private Bitmap weatherIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        username = findViewById(R.id.signin_username);
        email = findViewById(R.id.signin_email);
        password = findViewById(R.id.signin_password);
        signin = findViewById(R.id.signin_button_signin);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            weatherIcon = (Bitmap) extras.get("weatherIcon");
        }


        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(username.getText())) {
                    Toast.makeText(getApplicationContext(),"Enter username",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(email.getText())) {
                    Toast.makeText(getApplicationContext(),"Enter email",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(password.getText())) {
                    Toast.makeText(getApplicationContext(),"Enter password",Toast.LENGTH_SHORT).show();
                    return;
                }

                testSignin(username.getText().toString(), email.getText().toString(), password.getText().toString());

            }
        });

    }


    public void testSignin(String pseudo, String email, String password) {

        String url ="http://52.4.247.20:8081/inscription";

        String jsonStr = "{\"pseudo\":\""+pseudo+"\",\"email\":\""+email+"\",\"password\":\""+password+"\"}";
        JSONObject json = null;
        try {
            json = new JSONObject(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("INSCRIPTION",response.get("pseudo") + " is connected");
                            Intent mainActivityIntent = new Intent(SigninActivity.this, MainActivity.class);
                            mainActivityIntent.putExtra("userId",(int) response.get("id"));
                            mainActivityIntent.putExtra("userPseudo", (String) response.get("pseudo"));
                            mainActivityIntent.putExtra("weatherIcon",(Bitmap) weatherIcon);
                            startActivity(mainActivityIntent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("INSCRIPTION","ERREUR");
                        Toast.makeText(getApplicationContext(),"Pseudo or email already used",Toast.LENGTH_LONG).show();
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }


}