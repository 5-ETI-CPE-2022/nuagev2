package com.example.nuages;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.nuages.databinding.CommentsListFragmentBinding;
import com.example.nuages.databinding.UserPicturesListFragmentBinding;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class UserPicturesListFragment extends Fragment {


    private int userId;
    private int visitUserId;
    private String photoId;
    private String photoPath;

    public List<String> filePathList = new ArrayList<String>();
    public List<String> idList = new ArrayList<String>();
    public List<String> idPhotographerList = new ArrayList<String>();



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        UserPicturesListFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.user_pictures_list_fragment,container,false);
        binding.userPicturesList.setLayoutManager(new GridLayoutManager(
                binding.getRoot().getContext(), 3));

        userId = this.getArguments().getInt("userId");
        visitUserId = this.getArguments().getInt("visitUserId");


        getUserPictures(new VolleyCallBack() {
            @Override
            public void onSuccess() throws JSONException {

                initList(binding);

            }
        });


        binding.userPicturesList.setAdapter(new UserPicturesListAdapter(new ArrayList<>(), userId, visitUserId, photoId));
        return binding.getRoot();
    }

    void initList(UserPicturesListFragmentBinding binding) throws JSONException {
        List<CloudPicture> fakeList = new ArrayList<>();
        for (int i = filePathList.size()-1; i >= 0; i--) {
            CloudPicture cloudPicture = new CloudPicture();
            cloudPicture.setFilePath(filePathList.get(i));
            cloudPicture.setId(idList.get(i));
            cloudPicture.setIdPhotographer(Integer.parseInt(idPhotographerList.get(i)));

            fakeList.add(cloudPicture);
        }

        binding.userPicturesList.setAdapter(new UserPicturesListAdapter(fakeList, userId, visitUserId, photoId));
    }


    public void getUserPictures(final VolleyCallBack callBack) {


        String url ="http://52.4.247.20:8082/storage/dowloadImage/user/" + visitUserId;


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        for(int i=0;i<response.length();i++) {

                            try {
                                filePathList.add("https://"+response.getJSONObject(i).get("url").toString());
                                idList.add(response.getJSONObject(i).get("id").toString());
                                idPhotographerList.add(response.getJSONObject(i).get("id_user").toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        try {
                            callBack.onSuccess();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("USER PICTURES","ERREUR");
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(getActivity().getApplicationContext()).addToRequestQueue(jsonArrayRequest);
    }

}
