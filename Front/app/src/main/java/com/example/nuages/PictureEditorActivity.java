package com.example.nuages;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PictureEditorActivity extends AppCompatActivity {

    private ImageView picture;
    private ImageButton save;
    private ImageButton revert;
    private ImageButton delete;

    private LinearLayout drawing_pad;
    private OneOneFrameLayout picture_editor;

    private TextView tagView;

    private Uri mImageUri;
    private int pictureId;
    private String picture_path;
    private String origin;

    private int userId;
    private String userPseudo;
    private Bitmap weatherIcon;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_editor);

        picture = findViewById(R.id.picture_editor_picture_image);
        save = findViewById(R.id.picture_editor_button_save);
        revert = findViewById(R.id.picture_editor_button_revert);
        delete = findViewById(R.id.picture_editor_button_delete);

        drawing_pad = findViewById(R.id.view_drawing_pad);
        picture_editor = findViewById(R.id.picture_editor_picture);

        tagView = findViewById(R.id.picture_editor_tag);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            weatherIcon = (Bitmap) extras.get("weatherIcon");
            userId = (int) extras.get("userId");
            userPseudo = (String) extras.get("userPseudo");
            //The key argument here must match that used in the other activity
            origin = (String) extras.get("origin");
            if (origin.equals("myself")) {
                mImageUri = (Uri) extras.get("uri");
                grabImage(picture, mImageUri);
            } else {
                pictureId = (int) extras.get("pictureId");
                picture_path = (String) extras.get("picture_path");
                showPicture(picture_path);
            }
        } else {
            //finish();
        }


        ImageView weatherImageView = findViewById(R.id.weatherIcon_editor);
        weatherImageView.setImageBitmap(weatherIcon);


        revert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pictureEditorActivityIntent = new Intent(PictureEditorActivity.this, PictureEditorActivity.class);
                if (origin.equals("myself")) {
                    pictureEditorActivityIntent.putExtra("uri",mImageUri);
                } else {
                    pictureEditorActivityIntent.putExtra("pictureId",pictureId);
                    pictureEditorActivityIntent.putExtra("picture_path",picture_path);
                }
                pictureEditorActivityIntent.putExtra("origin",origin);
                pictureEditorActivityIntent.putExtra("userId", userId);
                pictureEditorActivityIntent.putExtra("userPseudo", userPseudo);
                pictureEditorActivityIntent.putExtra("weatherIcon", weatherIcon);
                finish();
                overridePendingTransition(0, 0);
                startActivity(pictureEditorActivityIntent);
                overridePendingTransition(0, 0);
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivityIntent = new Intent(PictureEditorActivity.this, MainActivity.class);
                mainActivityIntent.putExtra("userId", userId);
                mainActivityIntent.putExtra("userPseudo", userPseudo);
                mainActivityIntent.putExtra("weatherIcon", weatherIcon);
                finish();
                overridePendingTransition(0, 0);
                startActivity(mainActivityIntent);
                overridePendingTransition(0, 0);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //savePictureDrawing();
                picture.setDrawingCacheEnabled(true);
                picture_editor.setDrawingCacheEnabled(true);

                if (origin.equals("myself")) {
                    uploadBitmap(picture.getDrawingCache());
                } else {
                    uploadBitmapDrawing(picture_editor.getDrawingCache(), pictureId);
                }

                //picture.destroyDrawingCache();
                //picture_editor.destroyDrawingCache();

                Intent mainActivityIntent = new Intent(PictureEditorActivity.this, MainActivity.class);
                mainActivityIntent.putExtra("userId", userId);
                mainActivityIntent.putExtra("userPseudo", userPseudo);
                mainActivityIntent.putExtra("weatherIcon", weatherIcon);
                startActivity(mainActivityIntent);
            }
        });


        DrawingView mDrawingView=new DrawingView(this);
        //setContentView(R.layout.activity_picture_editor);
        LinearLayout mDrawingPad=(LinearLayout)findViewById(R.id.view_drawing_pad);
        mDrawingPad.addView(mDrawingView);



    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void grabImage(ImageView imageView, Uri mImageUri)
    {
        //this.getContentResolver().notifyChange(mImageUri, null);
        ContentResolver cr = this.getContentResolver();
        Bitmap bitmap;
        try
        {
            bitmap = handleSamplingAndRotationBitmap(this.getApplicationContext(), mImageUri);
            /////bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
            imageView.setImageBitmap(bitmap);
            //LinearLayout ll = findViewById(R.id.view_drawing_pad);
            //ll.setBackground(new BitmapDrawable(getApplicationContext().getResources(), bitmap));

        }
        catch (Exception e)
        {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
        }
    }





    /**
     * This method is responsible for solving the rotation issue if exist. Also scale the images to
     * 1024x1024 resolution
     *
     * @param context       The current context
     * @param selectedImage The Image URI
     * @return Bitmap image results
     * @throws IOException
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    /**
     * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
     * the closest inSampleSize that will result in the final decoded bitmap having a width and
     * height equal to or larger than the requested width and height. This implementation does not
     * ensure a power of 2 is returned for inSampleSize which can be faster when decoding but
     * results in a larger bitmap which isn't as useful for caching purposes.
     *
     * @param options   An options object with out* params already populated (run through a decode*
     *                  method with inJustDecodeBounds==true
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }


    private void savePictureDrawing() {


        /*
            Save Square Picture
         */
        picture.setDrawingCacheEnabled(true);
        String picSaved = MediaStore.Images.Media.insertImage(
                getContentResolver(), picture.getDrawingCache(), UUID.randomUUID()
                        .toString() + ".png", "picture");
        /*
        if (picSaved != null)
        {
            Toast savedToast = Toast.makeText(getApplicationContext(),
                    "Picture saved !", Toast.LENGTH_SHORT);
            savedToast.show();
        } else {
            Toast unsavedToast = Toast.makeText(getApplicationContext(),
                    "Picture NOT saved.", Toast.LENGTH_SHORT);

            unsavedToast.show();
        }
        */
        picture.destroyDrawingCache();


        /*
            Save Drawing Only
         */
        drawing_pad.setDrawingCacheEnabled(true);
        String drawSaved = MediaStore.Images.Media.insertImage(
                getContentResolver(), drawing_pad.getDrawingCache(), UUID.randomUUID()
                        .toString() + ".png", "drawing");
        /*
        if (drawSaved != null)
        {
            Toast savedToast = Toast.makeText(getApplicationContext(),
                    "Drawing saved !", Toast.LENGTH_SHORT);
            savedToast.show();
        } else {
            Toast unsavedToast = Toast.makeText(getApplicationContext(),
                    "Drawing NOT saved.", Toast.LENGTH_SHORT);

            unsavedToast.show();
        }
        */
        drawing_pad.destroyDrawingCache();



        /*
            Save Picture + Drawing
         */
        picture_editor.setDrawingCacheEnabled(true);
        String picdrawSaved = MediaStore.Images.Media.insertImage(
                getContentResolver(), picture_editor.getDrawingCache(), UUID.randomUUID()
                        .toString() + ".png", "picture_drawing");
        /*
        if (picdrawSaved != null)
        {
            Toast savedToast = Toast.makeText(getApplicationContext(),
                    "Picture+Drawing saved !", Toast.LENGTH_SHORT);
            savedToast.show();
        } else {
            Toast unsavedToast = Toast.makeText(getApplicationContext(),
                    "Picture+Drawing NOT saved.", Toast.LENGTH_SHORT);

            unsavedToast.show();
        }
        */
        picture_editor.destroyDrawingCache();

    }


    public void showPicture(String url) {

        // Retrieves an image specified by the URL, displays it in the UI.
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        picture.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        // ERROR
                    }
                });
        Log.e("REQUEST",request.toString());
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(request);
    }

    /*
    public void uploadPicture(String picture_file, String id_user) {

        String url = "http://52.4.247.20:8082/storage/uploadImage";

        String jsonStr = "{\"file\":\""+picture_file+"\",\"id_user\":\""+id_user+"\"}";
        JSONObject json = null;
        try {
            json = new JSONObject(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("UPLOAD Photo","Ok");
                        //uploadDrawing(picture_editor.getDrawingCache(), id_user, response.getString("id"))
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("UPLOAD Photo","Erreur");
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    public void uploadDrawing(Bitmap drawing_file, String id_user, String id_image) {

        String url = "http://52.4.247.20:8083/storage/uploadDessin";

        String jsonStr = "{\"file\":\""+drawing_file+"\",\"id_image\":\""+id_image+"\",\"id_user\":\""+id_user+"\"}";
        JSONObject json = null;
        try {
            json = new JSONObject(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("UPLOAD Dessin","Ok");
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("UPLOAD Dessin","Erreur");
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }


    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;

    }

    */


    /*
     * The method is taking Bitmap as an argument
     * then it will return the byte[] array for the given bitmap
     * and we will send this array to the server
     * here we are using PNG Compression with 80% quality
     * you can give quality between 0 to 100
     * 0 means worse quality
     * 100 means best quality
     * */
    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }



    private void uploadBitmap(final Bitmap bitmap) {

        String url = "http://52.4.247.20:8082/storage/uploadImage";

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        String json = null;
                        try {
                            json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        Log.e("IMAGE", String.valueOf(Integer.parseInt(json)));
                        if (Integer.parseInt(json) == 0) {
                            Toast.makeText(getApplicationContext(), "Picture is not a cloud !", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Picture uploaded !", Toast.LENGTH_SHORT).show();
                            Log.e("IMAGE","Response");
                            uploadBitmapDrawing(picture_editor.getDrawingCache(), Integer.parseInt(json));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(userId));
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("file", new DataPart(imagename + ".jpeg", getFileDataFromDrawable(bitmap)));
                return params;
            }

        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Log.e("IMAGE","Ok");
        //adding the request to volley
        MySingleton.getInstance(this).addToRequestQueue(volleyMultipartRequest);
    }


    private void uploadBitmapDrawing(final Bitmap bitmap, int id_picture) {

        String url = "http://52.4.247.20:8083/storage/uploadDessin";

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Toast.makeText(getApplicationContext(), "Drawing uploaded !", Toast.LENGTH_SHORT).show();
                        Log.e("DESSIN","Response");
                        /*
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));

                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                         */
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(userId));
                params.put("id_image", String.valueOf(id_picture));
                params.put("tag",tagView.getText().toString());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("file", new DataPart(imagename + ".jpeg", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };


        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Log.e("DESSIN","Ok");
        //adding the request to volley
        MySingleton.getInstance(this).addToRequestQueue(volleyMultipartRequest);
    }


    public void uploadPictureDrawing() {
        Bitmap bitmap = picture.getDrawingCache();
        uploadBitmap(bitmap);
    }


}