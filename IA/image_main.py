from flask import Flask
from tensorflow.keras import utils #pip install tensorflow
from tensorflow import keras
import matplotlib.pyplot as plt #pip install matplotlib
import tensorflow as tf
import numpy as np
from PIL import Image
from urllib import request
from io import BytesIO
from flask import request as rq
from keras.preprocessing import image

app = Flask(__name__)

@app.route('/bool_cloud')
def index():
	#path_tester = "https://static3.depositphotos.com/1001594/133/i/950/depositphotos_1337620-stock-photo-heavenly-heart-ii.jpg"
	path_tester = rq.args.get('url')
	path_tester = str(path_tester)
	img_height = 400
	img_width = 400
	batch_size = 16
	seed = 40
	# Define a simple sequential model
	def create_model():
	    num_classes = 2

	    model = tf.keras.Sequential([
	    tf.keras.layers.Rescaling(1./255),
	    tf.keras.layers.Conv2D(32, 3, activation='relu'),
	    tf.keras.layers.MaxPooling2D(),
	    tf.keras.layers.Conv2D(32, 3, activation='relu'),
	    tf.keras.layers.MaxPooling2D(),
	    tf.keras.layers.Conv2D(32, 3, activation='relu'),
	    tf.keras.layers.MaxPooling2D(),
	    tf.keras.layers.Flatten(),
	    tf.keras.layers.Dense(128, activation='relu'),
	    tf.keras.layers.Dense(num_classes)
	    ])

	    model.compile(
	    optimizer='adam',
	    loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
	    metrics=['accuracy'])

	    return model


	# Loads the weights
	checkpoint_path = "./cp1.ckpt"
	model = create_model()
	model.load_weights(checkpoint_path)


	#import image
	res = request.urlopen(path_tester).read()
	img = Image.open(BytesIO(res))
	img = img.convert('RGB')
	target_size=(img_width, img_height)
	img = img.resize(target_size, Image.NEAREST)
	img = image.img_to_array(img)
	img = np.expand_dims(img, axis=0)
	images = np.vstack([img])


	labels = ["True", "False"]  
	classes = model.predict(images, batch_size=10)
	res = np.argmax(classes[0])

	return labels[res]


@app.route('/predict_form')
def index2():
	#path_tester = "https://static3.depositphotos.com/1001594/133/i/950/depositphotos_1337620-stock-photo-heavenly-heart-ii.jpg"
	path_tester = rq.args.get('url')
	path_tester = str(path_tester)
	img_height = 400
	img_width = 400
	batch_size = 16
	seed = 40
	# Define a simple sequential model
	def create_model():
	    num_classes = 19

	    model = tf.keras.Sequential([
	    tf.keras.layers.Rescaling(1./255),
	    tf.keras.layers.Conv2D(32, 3, activation='relu'),
	    tf.keras.layers.MaxPooling2D(),
	    tf.keras.layers.Conv2D(32, 3, activation='relu'),
	    tf.keras.layers.MaxPooling2D(),
	    tf.keras.layers.Conv2D(32, 3, activation='relu'),
	    tf.keras.layers.MaxPooling2D(),
	    tf.keras.layers.Flatten(),
	    tf.keras.layers.Dense(128, activation='relu'),
	    tf.keras.layers.Dense(num_classes)
	    ])

	    model.compile(
	    optimizer='adam',
	    loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
	    metrics=['accuracy'])

	    return model


	# Loads the weights
	checkpoint_path = "./cp2.ckpt"
	model = create_model()
	model.load_weights(checkpoint_path)


	#import image
	res = request.urlopen(path_tester).read()
	img = Image.open(BytesIO(res))
	img = img.convert('RGB')
	target_size=(img_width, img_height)
	img = img.resize(target_size, Image.NEAREST)
	img = image.img_to_array(img)
	img = np.expand_dims(img, axis=0)
	images = np.vstack([img])

	labels = ["l'ours", 'le livre', 'le bracelet', 'le pont', 'le papillon', 'la bougie', 'la carotte', 'le crayon', 'le dauphin', "l'oreille", 'le poisson', 'la girafe', 'la guitare', 'le lion', 'la sirène', 'la chouette', "l'ananas", 'la chaussure', "l'araignée"]  
	classes = model.predict(images, batch_size=10)
	res = np.argmax(classes[0])
	return labels[res]

if __name__ == "__main__":
    app.run(host="0.0.0.0")
