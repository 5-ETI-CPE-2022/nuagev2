package com.nuage.image.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.nuage.image.model.Image;

public interface ImageRepository extends CrudRepository<Image,Integer> {
	@Query("SELECT i FROM Image i WHERE i.id_user = ?1")
	Optional<List<Image>> findByUser(int id);
	
	@Query(nativeQuery = true, value = "SELECT * FROM Image order by id desc limit :number ")
	Optional<List<Image>> getlastimages(@Param("number") int number);

	@Query(nativeQuery = true, value = "SELECT i.id FROM Image i order by id desc limit 1")
	int getLastID();

	@Query(nativeQuery = true,value= "SELECT i.tag FROM image i WHERE i.id=:id")
	String getTagByID(@Param("id") int id);
}
