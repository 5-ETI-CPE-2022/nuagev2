package com.nuage.image.controler;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.nuage.image.model.Image;
import com.nuage.image.model.ImageDTO;
import com.nuage.image.service.AmazonImageClient;
import com.nuage.image.service.ImageStorageInfoService;

@RestController
@RequestMapping("/storage/")
public class BucketController {
	
    private AmazonImageClient amazonClient;
    private ImageStorageInfoService imageService;
    
    @Autowired
    BucketController(AmazonImageClient amazonClient,ImageStorageInfoService imageService) {
        this.amazonClient = amazonClient;
        this.imageService=imageService;
    }

    @PostMapping("/uploadImage")
    public int uploadImage(@RequestPart(value = "file") MultipartFile file,@RequestParam String id) {
    	System.out.println("on passe par ici pour upload une image");
        return this.amazonClient.uploadFile(file,Integer.parseInt(id));
    }

    /*@DeleteMapping("/deleteFile")
    public String deleteFile(@RequestPart(value = "url") String fileUrl) {
        return this.amazonClient.deleteFileFromS3Bucket(fileUrl);
    }*/
    
    @GetMapping("/dowloadImage/user/{id}")
    public List<Image> dowloadImageByUser(@PathVariable String id){
    	return this.imageService.imageFromUser(Integer.parseInt(id));
    }
    
    @GetMapping("/dowloadImage/image/{id}")
	public Image dowloadImageById(@PathVariable String id) throws IOException{
		/*HttpHeaders headers = new HttpHeaders();
    	return new ResponseEntity<>(this.amazonClient.dowloadImage(Integer.parseInt(id)), headers, HttpStatus.OK);*/
		return this.imageService.imageById(Integer.parseInt(id)).orElse(null);
    }
    
    @GetMapping("/lastimages/{number}")
    public List<Image> getLastImages(@PathVariable String number) {
    	return this.imageService.getlastimages(Integer.parseInt(number));
    }
    
    @GetMapping("/lastimageswithdessins/{number}")
    public List<ImageDTO> imageswithinfo(@PathVariable String number) throws NumberFormatException, URISyntaxException, IOException, InterruptedException{
    	return this.imageService.getlastimageswithinfo(Integer.parseInt(number));
    }
    @GetMapping("/deleteimage/{id}")
    public void delete(@PathVariable String id){
	this.imageService.delete(Integer.parseInt(id));
    }
    @GetMapping("/getTag/{id}")
    public String getTag(@PathVariable String id){
    	return this.imageService.getTag(Integer.parseInt(id));
    }
  
}
