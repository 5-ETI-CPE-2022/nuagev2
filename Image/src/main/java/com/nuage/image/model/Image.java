package com.nuage.image.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Image {
	public Image(String url, int id,String tag) {
		this.url=url;
		this.id_user=id;
		this.tag=tag;
	}
	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	@Id
	@GeneratedValue
	private int id;
	private String url;
	private int id_user;
	private String tag;
	
	public Image() {
		super();
	}

	@Override
	public String toString() {
		return this.id+this.url+this.id_user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	
}
