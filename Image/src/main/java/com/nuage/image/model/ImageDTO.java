package com.nuage.image.model;

import java.io.Serializable;
import java.util.List;

public class ImageDTO implements Serializable{
	private static final long serialVersionUID = 1069270118228032176L;
	private int id;
	private String url;
	private String pseudo_user;
	private String tag;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	private Dessin associatedDessin;
	public ImageDTO(int id, String url, String pseudo_user,String tag, Dessin associatedDessin) {
		super();
		this.id = id;
		this.url = url;
		this.pseudo_user = pseudo_user;
		this.tag=tag;
		this.associatedDessin = associatedDessin;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPseudo_user() {
		return pseudo_user;
	}
	public void setPseudo_user(String pseudo_user) {
		this.pseudo_user = pseudo_user;
	}
	public Dessin getAssociatedDessin() {
		return associatedDessin;
	}
	public void setAssociatedDessin(Dessin associatedDessin) {
		this.associatedDessin = associatedDessin;
	}

}
