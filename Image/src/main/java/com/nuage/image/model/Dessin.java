package com.nuage.image.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Dessin {
	
	@Id
	@GeneratedValue
	private int id;
	private String url;
	private int id_user;
	private int id_image;
	@ElementCollection
	private Set<Integer> likes = new HashSet<>();
	private String tag;

	public Set<Integer> getLikes() {
		return likes;
	}
	public void setLike(Set<Integer> likes) {
		this.likes=likes;
	}
	public void addLike(Integer id_user) {
		this.likes.add(id_user);
	}
	public void removeLike(int id_user) {
		this.likes.remove(id_user);
	}
	
	public Dessin(String url, int id_user, int id_image, String tag) {
		super();
		this.url = url;
		this.id_user = id_user;
		this.id_image = id_image;
		this.tag = tag;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_image() {
		return id_image;
	}
	public void setId_image(int id_image) {
		this.id_image = id_image;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public Dessin() {
		super();
	}
}
