package com.nuage.image.model;

import java.util.HashSet;
import java.util.Set;
import java.io.Serializable;


public class User implements Serializable{
    private int id;
	private String email;
	private String pseudo;
	private String password;
	private String image;
	private Set<Integer> ListAbonnements = new HashSet<>();
	public User() {
		
	}
	
	public User(int id,String email, String pseudo, String password, String image) {
		this.id = id;
		this.email = email;
		this.pseudo = pseudo;
		this.password = password;
		this.image = image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Set<Integer> getListAbonnements() {
		return ListAbonnements;
	}

	public void setListAbonnements(Set<Integer> listAbonnements) {
		ListAbonnements = listAbonnements;
	}
	
}
