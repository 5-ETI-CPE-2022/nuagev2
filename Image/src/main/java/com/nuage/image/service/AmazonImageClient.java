package com.nuage.image.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.nuage.image.model.Image;

import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.InstanceProfileCredentialsProvider;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

@Service
public class AmazonImageClient {
	private S3Client s3client;
	@Autowired
    private ImageStorageInfoService imageService;

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;
    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    @PostConstruct
    private void initializeAmazon() {
    	/*AwsSessionCredentials credentials = AwsSessionCredentials.create(this.accessKey,this.secretKey,this.tokenKey);
    	System.out.println(this.accessKey+'\n'+this.secretKey+'\n');*/
    	S3Client amazonS3Client = S3Client.builder().credentialsProvider(InstanceProfileCredentialsProvider.create()).region(Region.US_EAST_1).build();
       this.s3client = amazonS3Client;
    }
    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
    
    private String generateFileName(MultipartFile multiPart) {
        return new Date().getTime() + "-" + multiPart.getOriginalFilename().replace(" ", "_");
    }
    
    private void uploadFileTos3bucket(String fileName, File file) {
        s3client.putObject(PutObjectRequest.builder().bucket(this.bucketName).key(fileName).build(),RequestBody.fromFile(file));
    }
    
    public int uploadFile(MultipartFile multipartFile,int id_user) {

        String fileUrl = "";
        int id=0;
        try {
            File file = convertMultiPartToFile(multipartFile);
            String fileName = generateFileName(multipartFile);
            fileUrl = endpointUrl + "/" + bucketName + "/" + fileName;
            uploadFileTos3bucket(fileName, file);
            file.delete();
            URL url = new URL("http://34.202.90.57:5000/bool_cloud?url=https://"+fileUrl);
            System.out.println("checking if this is a nuage");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line = reader.readLine();
            System.out.println(line);
            if(line.equals("False")){
            	this.deleteFileFromS3Bucket(fileName);
            	id=0;
            }
            else{
            	url=new URL("http://34.202.90.57:5000/predict_form?url=https://"+fileUrl);
	            con = (HttpURLConnection) url.openConnection();
	            con.setRequestMethod("GET");
	            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
	            String tag = reader.readLine();
                id= this.imageService.addImage(new Image(fileUrl,id_user,tag));
	    }
        } catch (Exception e) {
           e.printStackTrace();
        }
        System.out.println(id);
        return id;
    }
	/*public @ResponseBody byte[] dowloadImage(int id) throws IOException {
		Optional<Image> imageModel= this.imageService.imageById(id);
		if (imageModel.isPresent()) {
			ResponseInputStream<GetObjectResponse> object = this.s3client.getObject(GetObjectRequest.builder()
				    .bucket(this.bucketName)
				    .key(imageModel.get().getUrl())
				    .build());
			return IOUtils.toByteArray(object);
		}
		return null;
	}*/
    
    public String deleteFileFromS3Bucket(String filename) throws Exception {
        DeleteObjectRequest request = DeleteObjectRequest.builder().bucket(bucketName).key(filename).build();
        try {
            s3client.deleteObject(request);
        }catch (Exception e) {
            throw new Exception("Error deleting bundle version from S3 due to: " + e.getMessage(), e);
        }
        return "Successfully deleted";
    }
}
