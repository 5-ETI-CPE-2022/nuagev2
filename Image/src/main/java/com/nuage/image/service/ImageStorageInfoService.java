package com.nuage.image.service;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.core.io.ResourceLoader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.nuage.image.model.Dessin;
import com.nuage.image.model.Image;
import com.nuage.image.model.ImageDTO;
import com.nuage.image.model.User;
import com.nuage.image.repository.ImageRepository;

@Service
public class ImageStorageInfoService {

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;
    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private ResourceLoader resourceLoader;

    public int addImage(Image image) {
	imageRepository.save(image);
	int last_id=this.imageRepository.getLastID();
	return last_id;
    }

    public List<Image> imageFromUser(int id) {
	return this.imageRepository.findByUser(id).orElse(null);
    }

	/*public byte[] imageById(int id) {
		Optional<Image> imageModel= this.imageRepository.findById(id);
		byte[] image=null;
		if (imageModel.isPresent()) {
			try {
				image = this.getImage("https://"+bucketName+"."+endpointUrl + "/" + imageModel.get().getUrl());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return image;
			}
		return image;
	}

	public byte[] getImage(String url) throws IOException {
        System.out.println("File to be accessed :: " + url);
        Resource resource = this.resourceLoader.getResource(url);
        return FileCopyUtils.copyToByteArray(resource.getInputStream());
    }*/

    public Optional<Image> imageById(int id) {
	return this.imageRepository.findById(id);
    }

    public List<Image> getlastimages(int number) {
	Optional<List<Image>> images = this.imageRepository.getlastimages(number);
	if (images.isPresent()) 
		return images.get();
	else return null;
    }

    public List<ImageDTO> getlastimageswithinfo(int number) throws URISyntaxException, IOException, InterruptedException {
	List<Image> images= this.getlastimages(number);
	List<ImageDTO> listimage=new ArrayList<>();
        ObjectMapper userMapper = new ObjectMapper();
        ObjectMapper dessinMapper = new ObjectMapper();
	dessinMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
 	dessinMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        userMapper.configure(SerializationFeature.CLOSE_CLOSEABLE, true);
	userMapper.configure(SerializationFeature.CLOSE_CLOSEABLE, true);
    	for(Image i:images) {
  		HttpRequest requestDessin = HttpRequest.newBuilder(new URI("http://52.4.247.20:8083/storage/getDessin/image/"+i.getId()+"/user/"+i.getId_user())).build();
                HttpResponse<String> responseDessin = HttpClient.newHttpClient().send(requestDessin, HttpResponse.BodyHandlers.ofString());
    		HttpRequest requestUser = HttpRequest.newBuilder(new URI("http://52.4.247.20:8081/user/"+i.getId_user())).build();
    		HttpResponse<String> responseUser = HttpClient.newHttpClient().send(requestUser, HttpResponse.BodyHandlers.ofString());
    		try {
			System.out.println(responseDessin.body().toString());
			System.out.println(responseUser.body().toString());
    			Dessin associatedDessin=dessinMapper.readValue(responseDessin.body().toString(), Dessin[].class)[0];
    			User associatedUser=userMapper.readValue(responseUser.body().toString(), User.class);
    			ImageDTO imageDTO=new ImageDTO(i.getId(),i.getUrl(),associatedUser.getPseudo(),i.getTag(),associatedDessin);
    			listimage.add(imageDTO);
    		}
    		catch (IOException e) {
	    	    e.printStackTrace();
	    	}
     	}
    	return listimage;
	}

    public void delete(int id){
	this.imageRepository.deleteById(id);
    }

	public String getTag(int id_image) {
		return this.imageRepository.getTagByID(id_image);
	}
}
