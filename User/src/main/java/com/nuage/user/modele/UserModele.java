package com.nuage.user.modele;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "users")
public class UserModele {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
	
	@Column(name="email")
	private String email = "";
	
	@Column(name="pseudo")
	private String pseudo = "";
	
	@Column(name="password")
	private String password = "";
	
	@Column(name="image")
	private String image = "";
	
	@ElementCollection
	private Set<Integer> ListAbonnements = new HashSet<>();

	
	public UserModele() {
		
	}
	
	public UserModele(String email, String pseudo, String password, String image) {
		this.email = email;
		this.pseudo = pseudo;
		this.password = password;
		this.image = image;
	}
	
	public UserModele(String email, String pseudo, String password) {
		this.email = email;
		this.pseudo = pseudo;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Set<Integer> getListAbonnements() {
		return ListAbonnements;
	}

	public void setListAbonnements(Set<Integer> listAbonnements) {
		ListAbonnements = listAbonnements;
	}

	@Override
	public String toString() {
		return "UserModele [id=" + id + ", email=" + email + ", pseudo=" + pseudo + ", password=" + password
				+ ", image=" + image + ", ListAbonnements=" + ListAbonnements + "]";
	}
	
}
