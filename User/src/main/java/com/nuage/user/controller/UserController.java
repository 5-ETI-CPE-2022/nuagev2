package com.nuage.user.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nuage.user.modele.UserModele;
import com.nuage.user.service.UserService;

@RestController
public class UserController {
	@Autowired
	private UserService userservice;
	
	
	@RequestMapping("/getUsers")
	private List<UserModele> getAllUsers() {
		return userservice.getAllUsers();

	}

	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/inscription")
	public UserModele Inscription(@RequestBody Map<String, String> input) {
		return userservice.Inscription(input.get("pseudo"),input.get("password"),input.get("email"));
	}

	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/connexion")
	public UserModele Connexion(@RequestBody Map<String, String> input) {
		return userservice.Connexion(input.get("email"),input.get("password"));
	}
	
	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/NouvelAbonnement")
	public void NouvelAbonnement(@RequestBody Map<String, String> input) {
		userservice.NouvelAbonnement(input.get("user_principal"),input.get("user_abonnement"));
	}

	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/SeDesabonner")
	public void SeDesabonner(@RequestBody Map<String, String> input) {
		userservice.SeDesabonner(input.get("user_principal"),input.get("user_abonnement"));
	}
	
	@RequestMapping("/user/{id}")
	private UserModele getUser(@PathVariable String id) {
		Optional<UserModele> user;
		user= userservice.GetUserById(Integer.valueOf(id));
		if(user.isPresent()) {
			return user.get();
		}
		return null;
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}")
	public UserModele updateUser(@RequestBody UserModele user,@PathVariable String id) {
		user.setId(Integer.valueOf(id));
		UserModele new_user = userservice.updateUser(user);
		
		if(!new_user.equals(user)) {
			return new_user;
		}
		return null;
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/user/{id}")
	public void deleteUser(@PathVariable String id) {
		userservice.deleteUser(id);
	}



}
