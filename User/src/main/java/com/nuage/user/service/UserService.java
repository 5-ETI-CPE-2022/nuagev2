package com.nuage.user.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuage.user.modele.UserModele;
import com.nuage.user.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public UserModele Inscription(String pseudo, String password, String email) {
		if (GetUserByPseudo(pseudo) == null && GetUserByEmail(email) == null) {
			UserModele u = new UserModele(email, pseudo, password);
			userRepository.save(u);
			UserModele u_db = userRepository.findByEmail(email);
			return u_db;
		}
		return null;
	}

	public UserModele Connexion(String email, String password) {
		if (GetPasswordByEmail(email).equals(password)) {
			return GetUserByEmail(email);
		}
		return null;
	}

	public void NouvelAbonnement(String user_principal, String user_abonnement) {
		int user_principal_int = Integer.parseInt(user_principal);
		int user_abonnement_int = Integer.parseInt(user_abonnement);

		if (GetUserById(user_principal_int).isPresent() && GetUserById(user_abonnement_int).isPresent()) {
			UserModele user = GetUserById(user_principal_int).get();
			if (user != null) {
				Set<Integer> list_abo = user.getListAbonnements();
				if (!list_abo.contains(user_abonnement_int)) {
					list_abo.add(user_abonnement_int);
					user.setListAbonnements(list_abo);
					userRepository.save(user);
				}
			}
		}
	}

	public void SeDesabonner(String user_principal, String user_abonnement) {
		int user_principal_int = Integer.parseInt(user_principal);
		int user_abonnement_int = Integer.parseInt(user_abonnement);

		if (GetUserById(user_principal_int).isPresent() && GetUserById(user_abonnement_int).isPresent()) {
			UserModele user = GetUserById(user_principal_int).get();
			if (user != null) {
				Set<Integer> list_abo = user.getListAbonnements();
				if (list_abo.contains(user_abonnement_int)) {
					list_abo.remove(user_abonnement_int);
					user.setListAbonnements(list_abo);
					userRepository.save(user);
				}
			}
		}
	}

	public UserModele GetUserByPseudo(String pseudo) {
		return userRepository.findByPseudo(pseudo);

	}

	public UserModele GetUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public String GetPasswordByEmail(String email) {
		return userRepository.findPasswordByEmail(email);
	}

	public Optional<UserModele> GetUserById(int id) {
		return userRepository.findById(id);
	}

	public List<UserModele> getAllUsers() {
		List<UserModele> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public UserModele updateUser(UserModele user) {
		Optional<UserModele> user_db = GetUserById(user.getId());
		if (user_db.isPresent() && GetUserByPseudo(user.getPseudo()) == null
				&& GetUserByEmail(user.getEmail()) == null) {
			UserModele user_to_save = user_db.get();
			if (!user_to_save.getEmail().equals(user.getEmail()) && !user.getEmail().equals("")) {
				user_to_save.setEmail(user.getEmail());
			}
			if (!user_to_save.getImage().equals(user.getImage()) && !user.getImage().equals("")) {
				user_to_save.setImage(user.getImage());
			}
			if (!user_to_save.getPassword().equals(user.getPassword()) && !user.getPassword().equals("")) {
				user_to_save.setPassword(user.getPassword());
			}
			if (!user_to_save.getPseudo().equals(user.getPseudo()) && !user.getPseudo().equals("")) {
				user_to_save.setPseudo(user.getPseudo());
			}
			if (!user_to_save.getListAbonnements().equals(user.getListAbonnements())
					&& !user.getListAbonnements().equals("")) {
				user_to_save.setListAbonnements(user.getListAbonnements());
			}
			userRepository.save(user_to_save);
			return user_to_save;
		}
		return user;
	}

}
