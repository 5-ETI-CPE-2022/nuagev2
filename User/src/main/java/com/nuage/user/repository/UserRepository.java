package com.nuage.user.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.nuage.user.modele.UserModele;

public interface UserRepository extends CrudRepository<UserModele, Integer> {
	
    UserModele findByPseudo(String pseudo);
    
    UserModele findByEmail(String email);
    
    @Query("select u.password from UserModele u where u.email=?1")
	public String findPasswordByEmail(String email);

    
    Set<Integer> findListAbonnementsById(int user);
    
}
